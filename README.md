# Environment Link fixer

This module provides an HTML filter and a link widget/formatter to
strip production domain names from links and images.

## [Link widget](./src/Plugin/Field/FieldWidget/EnvLinkFixerLinkWidget.php)

The widget will automatically strip the current domain name from an absolute URL
when saving a link field.

## [Link formatter](./src/Plugin/Field/FieldFormatter/EnvLinkFixerLinkFormatter.php)

The link formatter can have a custom mapping to use, or fallback to the general mapping.

## [Text filter](./src/Plugin/Filter/RewriteOwnDomainLinks.php)

The text filter can have a custom mapping to use, or fallback to the general mapping.

## Settings

You can use the following settings to control the behavior of the module

### Disable the module

The following (advised in production) will disable the modules logic.

```php
$settings['env_link_fixer_disabled'] = TRUE;
```

### Use local names

You can easily add your local development names. These will be merged
with the one defined in config.

```php
$settings['env_link_fixer_custom_mappings'] = [
  'local.example.com' => 'www.example.com,example.com',
  'local.example.net' => [
    'www.example.net',
    'example.net',
  ],
];
```
