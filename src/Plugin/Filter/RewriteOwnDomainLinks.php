<?php

namespace Drupal\env_link_fixer\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to rewrite URLs.
 *
 * @Filter(
 *   id = "env_link_fixer_strip_domain",
 *   title = @Translation("Convert absolute URLs to relative for some domains"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 99
 * )
 */
class RewriteOwnDomainLinks extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public $settings = [
    'domain_names' => '',
  ];

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (env_link_fixer_disabled()) {
      return new FilterProcessResult($text);
    }

    $domain_names = $this->settings['domain_names'] ?? '';
    if (empty($domain_names)) {
      $domain_names = [];
    }
    else {
      $domain_names = env_link_fixer_convert_storage_to_array($domain_names);
    }

    $text = env_link_fixer_strip_domain($text, '', $domain_names);

    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $help = '<h4>Convert absolute URLs to relative for same domain</h4>';

    return $help;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['domain_names'] = [
      '#type' => 'textarea',
      '#title' => $this->t('List of domain name mappings'),
      '#description' => $this->t('Format to use is hostname|domain name to strip, ex. local.example.com|www.example.com,example.com'),
      '#default_value' => $this->settings['domain_names'] ?? '',
    ];

    return $form;
  }

}
