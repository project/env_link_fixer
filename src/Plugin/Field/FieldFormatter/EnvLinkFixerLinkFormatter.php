<?php

namespace Drupal\env_link_fixer\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\link\LinkItemInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "env_link_fixer_link_formatter",
 *   label = @Translation("Format links (Env)"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class EnvLinkFixerLinkFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function defaultSettings() {
    return [
      'local_domains' => '',
      'force_relative' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['local_domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('List of domains considered local'),
      '#default_value' => $this->getSetting('local_domains'),
      '#description' => $this->t('Domain names on separate lines.'),
    ];
    $elements['force_relative'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force relative URLs'),
      '#default_value' => $this->getSetting('force_relative'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $settings = $this->getSettings();

    if (!empty($settings['force_relative'])) {
      $summary[] = $this->t('Local domains will be made relative');
    }

    return $summary;
  }

  /**
   * Builds the \Drupal\Core\Url object for a link field item.
   *
   * @param \Drupal\link\LinkItemInterface $item
   *   The link field item being rendered.
   *
   * @return \Drupal\Core\Url
   *   A Url object.
   */
  protected function buildUrl(LinkItemInterface $item) {
    if (env_link_fixer_disabled()) {
      return parent::buildUrl($item);
    }

    try {
      $url = $item->getUrl();
    }
    catch (\InvalidArgumentException $e) {
      $url = Url::fromRoute('<none>');
    }

    $settings = $this->getSettings();
    $options = $item->options;
    $options += $url->getOptions();

    // Convert URL to relative if needed.
    if (isset($settings['force_relative']) && !empty($settings['force_relative'])) {
      // Skip routed links.
      if (!$url->isRouted()) {
        $local_domains = $settings['local_domains'];
        if (empty($local_domains)) {
          $local_domains = env_link_fixer_domains_to_strip();
        }
        else {
          $local_domains = env_link_fixer_domains_to_strip('', env_link_fixer_convert_storage_to_array($local_domains));
        }

        // Domain of URL.
        $domain = parse_url($url->getUri(), PHP_URL_HOST);

        if (is_array($local_domains) && in_array($domain, $local_domains)) {
          $parts = parse_url($url->getUri());
          $uri = substr($url->getUri(), strpos($url->getUri(), $domain) + strlen($domain));
          $url = $url->fromUserInput($uri);

          $options['external'] = FALSE;
          $url->setAbsolute(FALSE);

          // Link can be converted to internal, if so add query and fragment.
          if (!empty($parts['query'])) {
            parse_str($parts['query'], $query);
            $options['query'] = $query;
          }
          if (!empty($parts['fragment'])) {
            $options['fragment'] = $parts['fragment'];
          }
        }
      }
    }

    // Add optional 'rel' attribute to link options.
    if (!empty($settings['rel'])) {
      $options['attributes']['rel'] = $settings['rel'];
    }
    // Add optional 'target' attribute to link options.
    if (!empty($settings['target'])) {
      $options['attributes']['target'] = $settings['target'];
    }
    $url->setOptions($options);

    return $url;
  }

}
