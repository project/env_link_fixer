<?php

namespace Drupal\env_link_fixer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provide settings.
 *
 * @codeCoverageIgnore
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'env_link_fixer_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'env_link_fixer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_type = NULL) {
    $config = $this->config('env_link_fixer.settings');

    $form['mapping'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapping of URLs'),
      '#default_value' => $config->get('mapping'),
      '#description' => $this->t("Format to use is hostname|domeinname to strip"),
      '#required' => FALSE,
      '#config' => [
        'key' => 'env_link_fixer.settings:mapping',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('env_link_fixer.settings');
    $config->set('mapping', $form_state->getValue('mapping'));
    $config->save();
    $this->messenger()->addMessage($this->t('Your settings have been saved.'));
  }

}
