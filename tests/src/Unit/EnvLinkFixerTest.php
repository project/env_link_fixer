<?php

namespace Drupal\Tests\env_link_fixer\Unit;

use Drupal\Tests\UnitTestCase;

require_once __DIR__ . '/../../../env_link_fixer.module';

/**
 * Test domain stripping.
 */
class EnvLinkFixerTest extends UnitTestCase {

  /**
   * Test domain stripping.
   *
   * @dataProvider listOfSnippets
   */
  public function testProcess(string $html, string $expected) {
    $this->assertSame($expected, env_link_fixer_strip_domain($html, 'dev.example.com', [
      'dev.example.com' => [
        'www.example.com',
        'example.com',
      ],
    ]));
  }

  /**
   * Provides HTML snippets.
   */
  public function listOfSnippets(): array {
    return [
      [
        '<ul><li><a href="https://example.com/page-1">Page 1</a></li></ul>',
        '<ul><li><a href="/page-1">Page 1</a></li></ul>',
      ],
      [
        '<ul><li><a href="https://example.net">External</a></li></ul>',
        '<ul><li><a href="https://example.net">External</a></li></ul>',
      ],
      [
        '<div><ul>
          <li><a href="https://example.com/page-1">Page</a></li>
          <li><a href="https://www.example.com/page-2">Page</a></li>
          <li><a href="https://example.com/page-3">Page</a><br>
        </ul>
        </div>',
        '<div><ul>
          <li><a href="/page-1">Page</a></li>
          <li><a href="/page-2">Page</a></li>
          <li><a href="/page-3">Page</a><br>
        </ul>
        </div>',
      ],
    ];
  }

}
