<?php

namespace Drupal\Tests\env_link_fixer\Kernel;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Common functions.
 */
class TestBase extends FieldKernelTestBase {

  /**
   * Inline dataproviders.
   *
   * @var bool
   *
   * @see https://www.drupal.org/project/drupal/issues/1411074
   */
  protected $inlineDataProvider = TRUE;

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'link',
    'system',
    'env_link_fixer',
  ];

  /**
   * Test configuration.
   *
   * @var array
   */
  protected $testConfig = [];

  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Add a datetime range field.
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => mb_strtolower($this->randomMachineName()),
      'entity_type' => 'entity_test',
      'type' => $this->testConfig['storage']['type'],
      'settings' => $this->testConfig['storage']['settings'],
    ]);
    $this->fieldStorage->save();

    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'entity_test',
      'required' => TRUE,
    ]);
    $this->field->save();

    $display_options = [
      'type' => $this->testConfig['display']['type'],
      'label' => 'hidden',
      'settings' => $this->testConfig['display']['settings'],
    ];
    EntityViewDisplay::create([
      'targetEntityType' => $this->field->getTargetEntityTypeId(),
      'bundle' => $this->field->getTargetBundle(),
      'mode' => 'default',
      'status' => TRUE,
    ])->setComponent($this->fieldStorage->getName(), $display_options)
      ->save();
  }

  /**
   * Render entity.
   */
  protected function renderIt($entity_type, $entity, $lang_code = 'en') {
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
    $build = $view_builder->view($entity, 'full', $lang_code);
    $output = \Drupal::service('renderer')->renderRoot($build);

    $output = $output->__toString();
    $output = preg_replace('/\s+/', ' ', $output);

    return $output;
  }

  /**
   * Fix data provider keys.
   */
  protected function fixProviderKeys(string $prefix, array $tests) : array {
    $output = [];
    foreach ($tests as $key => $data) {
      $output[$prefix . '::' . $key] = $data;
    }

    return $output;
  }

}
