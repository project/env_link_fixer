<?php

namespace Drupal\Tests\env_link_fixer\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Test formatter with settings.
 */
class FormatterWithGlobalSettingsDisabledTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'env_link_fixer',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->testConfig = [
      'storage' => [
        'type' => 'link',
        'settings' => [],
      ],
      'display' => [
        'type' => 'env_link_fixer_link_formatter',
        'settings' => [
          'force_relative' => '1',
        ],
      ],
    ];

    parent::setUp();

    $this->installConfig(['env_link_fixer']);

    $this->setSetting('env_link_fixer_custom_mappings', [
      'localhost' => 'www.xyzzy.net',
    ]);
    $this->setSetting('env_link_fixer_disabled', TRUE);
  }

  /**
   * Test link output.
   *
   * @x-dataProvider providerInternalLinks
   */
  public function testFormatterWithGlobalSettingsDisabled($input = NULL, $expected = NULL) {
    if ($this->inlineDataProvider) {
      $data = array_merge(
        $this->providerInternalLinks(),
      );

      foreach ($data as $name => $row) {
        $input = $row['input'];
        $expected = '<a href="' . $row['expected'] . '"';

        $field_name = $this->fieldStorage->getName();
        // Create an entity.
        $entity = EntityTest::create([
          'name' => $this->randomString(),
          $field_name => [
            'uri' => $input,
          ],
        ]);

        $this->assertStringContainsString($expected, (string) $this->renderIt('entity_test', $entity), $name);
      }
    }
    else {
      $expected = '<a href="' . $expected . '"';

      $field_name = $this->fieldStorage->getName();
      // Create an entity.
      $entity = EntityTest::create([
        'name' => $this->randomString(),
        $field_name => [
          'uri' => $input,
        ],
      ]);

      $this->assertStringContainsString($expected, (string) $this->renderIt('entity_test', $entity));
    }
  }

  /**
   * Provide test examples.
   */
  public function providerInternalLinks() {
    $tests = [
      'link 1' => [
        'input' => 'https://example.com',
        'expected' => 'https://example.com',
      ],
      'link 2' => [
        'input' => 'https://www.example.com?x=3',
        'expected' => 'https://www.example.com?x=3',
      ],
      'link 3' => [
        'input' => 'https://www.xyzzy.net/page-3',
        'expected' => 'https://www.xyzzy.net/page-3',
      ],
      'link 4' => [
        'input' => 'https://www.www.xyzzy.net/page-4',
        'expected' => 'https://www.www.xyzzy.net/page-4',
      ],
      'link 5' => [
        'input' => 'https://www.xyzzy.net/page-5?ref=www.xyzzy.net',
        'expected' => 'https://www.xyzzy.net/page-5?ref=www.xyzzy.net',
      ],
      'link 6' => [
        'input' => 'https://www.www.xyzzy.net/page-6#xyzzy',
        'expected' => 'https://www.www.xyzzy.net/page-6#xyzzy',
      ],
      'link 7' => [
        'input' => 'https://www.xyzzy.net/page-7?x=2&y=3#xyzzy',
        'expected' => 'https://www.xyzzy.net/page-7?x=2&amp;y=3#xyzzy',
      ],
    ];

    return $this->fixProviderKeys(__FUNCTION__, $tests);
  }

}
